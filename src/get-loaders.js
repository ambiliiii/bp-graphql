import DataLoader from 'dataloader';
import modules from './modules';

export default () => {
    let loaders = {};
    const keys = Object.keys(modules.loaders);
    keys.forEach((key) => {
        // eslint-disable-next-line security/detect-object-injection
        loaders = { ...loaders, [key]: new DataLoader(modules.loaders[key]) };
    });
    return loaders;
};
