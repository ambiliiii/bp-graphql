/* eslint-disable no-console */
import redis from 'redis';
import { promisify } from 'util';

const port = Number(process.env.REDIS_PORT) || 6379;
const host = process.env.REDIS_HOST || '127.0.0.1';

const hostReadOnly = process.env.REDIS_HOST_READ_ONLY || '127.0.0.1';
const portReadOnly = Number(process.env.REDIS_PORT_READ_ONLY) || 6379;

/* 
    Primary client is used for writing operations and the other client
    is used for the reading operations.
*/

const client = redis.createClient(port, host, {
    retry_strategy: (options) => Math.min(options.attempt * 100, 3000),
});
const readClient = redis.createClient(portReadOnly, hostReadOnly, {
    retry_strategy: (options) => Math.min(options.attempt * 100, 3000),
});

const get = promisify(readClient.get).bind(readClient);
const hget = promisify(readClient.hget).bind(readClient);

const del = promisify(client.del).bind(client);
const hset = promisify(client.hset).bind(client);
const hkeys = promisify(client.hkeys).bind(client);
const hdel = promisify(client.hdel).bind(client);
const expire = promisify(client.expire).bind(client);

// Redis client is configured

export default { get, del, hset, hget, hkeys, hdel, expire };
