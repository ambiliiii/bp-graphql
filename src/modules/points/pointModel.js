import mongoose from 'mongoose';

const { Schema } = mongoose;
const pointSchema = new Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
    },
    userid:{
        type:mongoose.Schema.Types.ObjectId
    },
    // postid:{
    //     type:mongoose.Schema.Types.ObjectId
    // },
    // tagid:{
    //     type:mongoose.Schema.Types.ObjectId
    // },
    // commentid:{
    //     type:mongoose.Schema.Types.ObjectId
    // },
    // likeid:{
    //     type:mongoose.Schema.Types.ObjectId
    // },
    point:{
        type:Number
    }

})

    
    export default mongoose.model('points', pointSchema, 'points');
