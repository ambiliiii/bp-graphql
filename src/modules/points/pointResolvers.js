import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { combineResolvers } from 'graphql-resolvers';
//import { isAuthenticated, isAdmin } from '../auth';
import emailTemplate from '../email/email-service';
import { models } from 'mongoose';
//import user from '.';
import post from '.';
import user from '../user';



const pointResolvers = {
    Query: {
        listpoint:async(parent,args,{models})=>{
            try{
            const listedpoints= await models.points.find({})
            console.log("listedpoint",listedpoints)
            return listedpoints;
            }
         catch (error) {
            console.log('error', error);
            return error;
        }
    }
 },
 Mutation:{
    createpoint: (parent,{data},context,info) => {
        try{
         return new models.points({ ...data}).save();
    
    } catch (error) {
        console.log('error', error);
        return error;
    }
},


},


};       
export default pointResolvers;