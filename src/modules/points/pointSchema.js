import { gql } from 'apollo-server-express';

export default gql`
type Point{
    _id:ID
    userid:ID
    point:Int
}
extend type Query {
    listpoint:[Point]!
}


input pointInput{
    userid:ID!
    postid:ID!
    point:Int

}


extend type Mutation {
    createpoint(data:pointInput):Point!
}

`;