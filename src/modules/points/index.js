import pointModel from './pointModel';
import pointSchema from './pointSchema';
import pointResolvers from './pointResolvers';

export default {
    pointModel,
    pointSchema,
    pointResolvers,

};