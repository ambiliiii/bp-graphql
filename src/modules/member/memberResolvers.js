import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { combineResolvers } from 'graphql-resolvers';
import { isAuthenticated, isAdmin,isAuthorizedmember } from '../auth';
import emailTemplate from '../email/email-service';
import { models } from 'mongoose';
//import user from '.';
import post from '.';
import user from '../user';
import group from '.';
import member from '.';

const memberResolvers = {
    Query: {
        listmember:combineResolvers(isAuthenticated,async(parent,args,{models})=>{
            try{
            const listedmember= await models.member.find({})
            console.log("listedmember",listedmember)
            return listedmember;
            }
         catch (error) {
            console.log('error', error);
            return error;
        }
 }),
    },
    Member:{
        Chat:async(parent,args,{models})=>{
          console.log("parent",parent)
          try{
              let foundchat=await models.chat.find({userid:parent.userid})
              console.log("foundchat",foundchat)
              return foundchat;
          
          }catch(error){
              console.log("error",error)
              throw new Error(err)
         }
        },
        User:async(parent,args,{models})=>{
            console.log("parent",parent)
            try{
                let foundUser=await models.user.findById(parent.userid)
                console.log("founduser",foundUser)
                return foundUser;
            
            }catch(error){
                console.log("error",error)
                throw new Error(err)
           }
          }
         },
 Mutation:{
    createmember:combineResolvers(isAuthenticated,async(parent,{data},{models,me},info) => {
        try{
         return new models.member({ ...data,userid:me._id}).save();
    
    } catch (error) {
        console.log('error', error);
        return error;
    }
}),

Deletemember:combineResolvers(isAuthorizedmember,async(parent,{_id},{models})=>{
    
    try{
        let deletedmember=await models.member.deleteOne({_id})
        console.log("deletedmember",deletedmember)
        return deletedmember;
    
    }catch(error){
        console.log("error",error)
        return error;
   }
  }),

}
};    
export default memberResolvers;