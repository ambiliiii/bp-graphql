import { gql } from 'apollo-server-express';

export default gql`
type Member{
    _id:ID
    userid:ID
    groupid:ID
    created_date:String
    Chat:[Chat]!
    User:User
}
extend type Query{
    listmember:[Member]!
}

input memberInput{
    groupid:ID
    
}



extend type Mutation {
    createmember(data:memberInput):Member!
    Deletemember(_id:ID):Member!
}



`;
