import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { combineResolvers } from 'graphql-resolvers';
import { isAuthenticated, isAdmin, isAuthorizedcommentowner } from '../auth';
import emailTemplate from '../email/email-service';
import { models } from 'mongoose';
import post from '.';
import user from '../user';
import comment from '.';

const commentResolvers = {
    Query: {
        listcomment:combineResolvers(isAuthenticated,async(parent,args,{models})=>{
            try{
            const listedcomment = await models.comment.find({})
            console.log("listedcomment",listedcomment)
            return listedcomment;
            }
         catch (error) {
            console.log('error', error);
            return error;
        }
 }),

getcomment:combineResolvers(isAuthenticated,async(parent,args,context,info)=>{
    console.log("args",args)
    try{
            const gotcomment = await models.comment.findById({_id:args._id});
            console.log("gotcomment",gotcomment)
              return gotcomment;
    } catch (error) {
        console.log('error', error);
        return error; 
    }
 }),
},
Comment:{
    User:async(parent,args,{models})=>{
      //console.log("parent",parent)
      try{
          let foundUsers=await models.user.find({_id:parent.userid})
          console.log("foundUsers",foundUsers)
          return foundUsers;
      
      }catch(error){
          console.log("error",error)
          throw new Error(err)
     }
    }
     },
     Comment:{
        Post:async(parent,args,{models})=>{
          console.log("parent",parent)
          try{
              let foundposts=await models.post.find({userid:parent.userid})
              console.log("foundposts",foundposts)
              return foundposts;
          
          }catch(error){
              console.log("error",error)
              throw new Error(err)
         }
        }
         },


    Mutation:{
        createcomment:combineResolvers(isAuthenticated,async (parent,{data},{models,me},info) => {
            //console.log("data",{data})

            try{
             let commentdata=await models.comment({ ...data,userid:me._id}).save();
             let postowner=await models.post.findById(data.postid);
             console.log("postowner",postowner)
             let pointofowner=await models.points({
                  point:2,
                  commentid:postowner.commentid,
                  postid:postowner._id,
                  userid:postowner.userid
            }).save();
            console.log("pointofowner",pointofowner)
           
                let pointofoDonator=await models.points({
                     point:1,
                     commentid:commentdata._id,
                     postid:commentdata.postid,
                     userid:commentdata.userid
               }).save();
               console.log("pointofDonator",pointofoDonator)
               let createnotification=await models.notification({
                senderid:commentdata.userid,
                receiverid:postowner.userid,
                postid:postowner._id,
                commentid:commentdata._id,
                notification:`${me.firstName} ${me.lastName} commented on your post`
            }).save();
            console.log("createnotification",createnotification)
               return commentdata;
        
        } catch (error) {
            console.log('error', error);
            return error;
        }
    }),

updatecomment:combineResolvers(isAuthorizedcommentowner, async (parent, { _id, data }, { models}) => {
    try {
        const fields = ['comments'];
        const updatedcommentDoc = await models.comment.findById(_id);
        for (let i = 0; i < fields.length; i += 1) {
            if (data[fields[i]]) updatedcommentDoc[fields[i]] = data[fields[i]];
        }
        updatedcommentDoc.updatedDate = new Date();
        console.log("updatedcommentDoc",updatedcommentDoc)
        return updatedcommentDoc.save();
    } catch (error) {
        console.log('error', error);
        return error;
    }
}),
Deletecomment:combineResolvers(isAuthorizedcommentowner,async(parent,{_id},{models})=>{
    
    try{
        let deletedcomment=await models.comment.deleteOne({_id})
        console.log("deletedcomment",deletedcomment)
        return deletedcomment;
    
    }catch(error){
        console.log("error",error)
        return error;
   }
  }),
    }
};

export default commentResolvers;