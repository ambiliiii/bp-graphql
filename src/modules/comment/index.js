import commentModel from './commentModel';
import commentSchema from './commentSchema';
import commentResolvers from './commentResolvers';


export default {
    commentModel,
    commentSchema,
    commentResolvers,
    
};