import mongoose from 'mongoose';

const { Schema } = mongoose;
const commentSchema = new Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
    },
    userid:{
        type:mongoose.Schema.Types.ObjectId,required:true
    },
    postid:{
        type:mongoose.Schema.Types.ObjectId,required:true
    },
    comments:{
        type: String,
        required: true,
    },
    created_date:{
        type: Date,
        default: Date.now,
        index: true,
    },
    point: {
        type:Number
    }

})

export default mongoose.model('comment', commentSchema, 'comment');
