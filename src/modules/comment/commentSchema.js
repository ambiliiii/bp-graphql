import { gql } from 'apollo-server-express';

export default gql`
type Comment{
    _id:ID
    userid:ID
    postid:ID
    comments:String
    created_date:String
    User:[User]!
    Post:[Post]!
    point:Int
}
extend type Query {
    listcomment:[Comment]!
    getcomment(_id:ID!):Comment!
}

input commentInput{
    postid:ID
    comments:String
}
input updatecommentInput{
    comments:String!
}



extend type Mutation {
    createcomment(data: commentInput): Comment
    updatecomment(_id:ID!,data:updatecommentInput):Comment!
    Deletecomment(_id:ID):Comment!

}
`;
