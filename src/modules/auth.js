import { skip, combineResolvers } from 'graphql-resolvers';

export const isAuthenticated = async (parent, args, context) => {
    //console.log("context.me",context.me)
    if (context.me) {
        const user = await context.models.user.findById(context.me.userId);
        if (!user.status) return new Error('User has no permissions');

        context.me = user;
        //console.log("user",user)
        return user ? skip : new Error('Not authenticated');
    }
    throw new Error('Not authenticated');
};

export const isAdmin = combineResolvers(isAuthenticated, async (parent, args, { me }) =>
    me.role === 'admin' ? skip : new Error('Not authenticated')
);

export const isAuthorizedpostowner = combineResolvers(isAuthenticated, async (parent, {_id}, { models,me }) =>{
  if (me.role === 'admin') return skip;
  const post= await models.post.findById({_id});
        if (!post) return new Error('post unavailable');
        if (post.userid.toString()===me._id.toString()) return skip;
        else return new Error('Not authorized');

});
export const isAuthorizedlikeowner = combineResolvers(isAuthenticated, async (parent, {_id}, { models,me }) =>{
    if (me.role === 'admin') return skip;
    const like= await models.like.findById({_id});
          if (!like) return new Error('like unavailable');
          if (like.userid.toString()===me._id.toString()) return skip;
          else return new Error('Not authorized');
  
  });
  export const isAuthorizedcommentowner = combineResolvers(isAuthenticated, async (parent, {_id}, { models,me }) =>{
    if (me.role === 'admin') return skip;
    const comment= await models.comment.findById({_id});
          if (!comment) return new Error('comment unavailable');
          if (comment.userid.toString()===me._id.toString()) return skip;
          else return new Error('Not authorized');
  
  });
  export const isAuthorizedmember= combineResolvers(isAuthenticated, async (parent, {_id}, { models,me }) =>{
    if (me.role === 'admin') return skip;
    const member= await models.member.findById({_id});
          if (!member) return new Error('member does not exist');
          if (member.userid.toString()===me._id.toString()) return skip;
          else return new Error('Not authorized');
  
  });
export const isAuthorizedchat= combineResolvers(isAuthenticated, async (parent, {_id}, { models,me }) =>{
    if (me.role === 'admin') return skip;
    const chat= await models.chat.findById({_id});
          if (!chat) return new Error(' chat unavailable');
          if (chat.userid.toString()===me._id.toString()) return skip;
          else return new Error('Not authorized');
  
  });


