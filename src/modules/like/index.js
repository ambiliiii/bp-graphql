import likeModel from './likeModel';
import likeSchema from './likeSchema';
import likeResolvers from './likeResolvers';

export default {
    likeModel,
    likeSchema,
    likeResolvers,
};