import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { combineResolvers } from 'graphql-resolvers';
import { isAuthenticated, isAdmin,isAuthorizedlikeowner } from '../auth';
import emailTemplate from '../email/email-service';
import { models } from 'mongoose';
//import user from '.';
import post from '.';
import user from '../user';
import like from '.';

const likeResolvers = {
    Query: {
        listlike:combineResolvers(isAuthenticated,async(parent,args,{models})=>{
            try{
            const listedlike = await models.like.find({})
            console.log("listedlike",listedlike)
            return listedlike;
            }
         catch (error) {
            console.log('error', error);
            return error;
        }
 }),

 getlike:combineResolvers(isAuthenticated,async(parent,args,context,info)=>{
    console.log("args",args)
    try{
            const gotlike = await models.like.findById({_id:args._id});
            console.log("gotlike",gotlike)
              return gotlike;
    } catch (error) {
        console.log('error', error);
        return error; 
    }
 }),
 listlikecount:async(parent,{_id},{models})=>{
    try{
        const listedlikecount= await models.like.find({}).count();
        console.log("listedlikecount",listedlikecount)
          return listedlikecount;
} catch (error) {
    console.log('error', error);
    return error; 
}
 }
    
 
},

    

    Mutation:{
        createlike:combineResolvers(isAuthenticated,async(parent,{data},{models,me},info) => {
            //console.log("data",{data})

            try{  
             let likesdata =await models.like({ ...data}).save();
             let postowner=await models.post.findById(data.postid);
             console.log("postowner",postowner)
             let pointofowner=await models.points({
                  point:1,
                  likeid:postowner.likeid,
                  postid:postowner._id,
                  userid:postowner.userid
            }).save();
            console.log("pointofowner",pointofowner)
            let pointofDonator=await models.points({
                point:1,
                likeid:likesdata._id,
                postid:likesdata.postid,
                userid:likesdata.userid
          }).save();
          console.log("pointofDonator",pointofDonator)
          let createnotification=await models.notification({
              senderid:likesdata.userid,
              receiverid:postowner.userid,
              postid:postowner._id,
              likeid:likesdata._id,
              notification:`${me.firstName} ${me.lastName} liked in your post` 
          }).save();
          console.log("createnotification",createnotification)
            return likesdata;
        } catch (error) {
            console.log('error', error);
            return error;
        }
    }),
    Deletelike:combineResolvers(isAuthorizedlikeowner,async(parent,{_id},{models})=>{
    
        try{
            let deletedlike=await models.like.deleteOne({_id})
            console.log("deletedlike",deletedlike)
            return deletedlike;
        
        }catch(error){
            console.log("error",error)
            return error;
       }
      }),

    }
};

export default likeResolvers;
