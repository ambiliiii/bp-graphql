import mongoose from 'mongoose';


const { Schema } = mongoose;
const likeSchema = new Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
    },
    userid:{
        type:mongoose.Schema.Types.ObjectId,required:true
    },
    postid:{
        type:mongoose.Schema.Types.ObjectId,
        //required:true
    },
    like_date:{
        type: Date,
        default: Date.now,
        index: true,
    },
    point: {
        type:Number
    },
    
 })

export default mongoose.model('like', likeSchema, 'like');