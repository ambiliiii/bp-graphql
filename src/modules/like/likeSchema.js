import { gql } from 'apollo-server-express';

export default gql`

type Like{
    _id:ID
    userid:ID
    postid:ID
    like_date:String
    point:Int
}
extend type Query {
    listlike:[Like]!
    getlike(_id:ID!):Like!
    listlikecount:Like
}
input likeInput{
    postid:ID
}

extend type Mutation {
    createlike(data:likeInput):Like
    Deletelike(_id:ID):Like
}

`;