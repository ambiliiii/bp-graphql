import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { combineResolvers } from 'graphql-resolvers';
import { isAuthenticated, isAdmin } from '../auth';
import emailTemplate from '../email/email-service';
import { models } from 'mongoose';
//import post from '.';
import user from '../user';
import comment from '.';
import designation from '.';

const designationResolvers = {
    Query: {
        listdesignation:async(parent,args,{models})=>{
            try{
            const listedDesignation = await models.designation.find({})
            console.log("listedDesignation",listedDesignation)
            return listedDesignation;
            }
         catch (error) {
            console.log('error', error);
            return error;
        }
 },

getdesignation:async(parent,args,context,info)=>{
    try{
            const gotdesignation = await models.designation.findById({_id:args._id});
              return gotdesignation;
    } catch (error) {
        console.log('error', error);
        return error; 
    }
 },
},
    Mutation:{
        createdesignation:(parent,{data},{models},info) => {
            console.log("data",{data})

            try{
             return new models.designation({ ...data}).save();
        
        } catch (error) {
            console.log('error', error);
            return error;
        }
    },


    }
};

export default designationResolvers;