import { gql } from 'apollo-server-express';

export default gql`

type Designation{
    _id:ID!
    title:String!

}
extend type Query{
    listdesignation:[Designation]!
    getdesignation(_id:ID!):Designation!
}


input designationInput{
    title:String!
}


extend type Mutation {
    createdesignation(data:designationInput):Designation!
}

`;