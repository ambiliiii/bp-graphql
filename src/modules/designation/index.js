import designationModel from './designationModel';
import designationSchema from './designationSchema';
import designationResolvers from './designationResolvers';

export default {
    designationModel,
    designationSchema,
    designationResolvers,
    
};