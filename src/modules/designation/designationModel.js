import mongoose from 'mongoose';

const { Schema } = mongoose;
const designationSchema = new Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
    },
    title:{
        type:String,required:true
    },
    

})


export default mongoose.model('designation', designationSchema, 'designation');
