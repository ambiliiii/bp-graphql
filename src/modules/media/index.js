import mediaModel from './mediaModel';
import mediaSchema from './mediaSchema';
import mediaResolvers from './mediaResolvers';

export default {
    mediaModel,
    mediaSchema,
    mediaResolvers
};