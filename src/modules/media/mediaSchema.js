import { gql } from 'apollo-server-express';

export default gql`
enum Posttype {
    photo
    video
}

type Media{
    _id:ID!
    postid:ID
    posttype:String
    Url:String
    
}
extend type Query {
    listmedia:[Media]!
}
input mediaInput{
    postid:ID
    posttype:String
    Url:String
}

extend type Mutation {
    createmedia(data:mediaInput):Media
}

`;