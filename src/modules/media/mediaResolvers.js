import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { combineResolvers } from 'graphql-resolvers';
import { isAuthenticated, isAdmin } from '../auth';
import emailTemplate from '../email/email-service';
import { models } from 'mongoose';
//import user from '.';
import post from '.';
import user from '../user';
import like from '.';
import media from '.';


const mediaResolvers = {
    Query: {
        listmedia:async(parent,args,{models})=>{
            try{
            const listedmedia = await models.media.find({})
            console.log("listedmedia",listedmedia)
            return listedmedia;
            }
         catch (error) {
            console.log('error', error);
            return error;
        }
 },
},
    Mutation:{
        createmedia:(parent,{data},{models},info) => {
            console.log("data",{data})
    
            try{
             return new models.media({ ...data}).save();
        
        } catch (error) {
            console.log('error', error);
            return error;
        }
    },
    },
    };
    
    export default mediaResolvers;