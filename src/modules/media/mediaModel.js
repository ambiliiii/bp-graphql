import mongoose from 'mongoose';

const { Schema } = mongoose;
const mediaSchema = new Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
    },
    postid:{
        type:mongoose.Schema.Types.ObjectId,
    },
    posttype:{
        type: String,
        enum: ['photo', 'video'],
        default: 'text',
    },
    Url:{
        type:String
    }
    

})

export default mongoose.model('media', mediaSchema, 'media');
