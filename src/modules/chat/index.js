import chatModel from './chatModel';
import chatSchema from './chatSchema';
import chatResolvers from './chatResolvers';

export default {
    chatModel,
    chatSchema,
    chatResolvers,

};