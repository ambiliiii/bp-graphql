import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { combineResolvers } from 'graphql-resolvers';
import { isAuthenticated, isAdmin,isAuthorizedchat } from '../auth';
import emailTemplate from '../email/email-service';
import { models } from 'mongoose';
//import post from '.';
import user from '../user';
import comment from '.';
import group from '.';
import chat from '.';



const chatResolvers = {
    Query: {
        listchat:combineResolvers(isAuthenticated,async(parent,args,{models})=>{
            try{
            const listedchats = await models.chat.find({})
            console.log("listedchats",listedchats)
            return listedchats;
            }
         catch (error) {
            console.log('error', error);
            return error;
        }
 }),
 getchat:combineResolvers(isAuthenticated,async(parent,args,context,info)=>{
    try{
            const gotchat = await models.chat.findById({_id:args._id});
              return gotchat;
    } catch (error) {
        console.log('error', error);
        return error; 
    }
 }),
},
Chat:{
    Member:async(parent,args,{models})=>{
      console.log("parent",parent)
      try{
          let foundmember=await models.member.find({userid:parent._id})
          console.log("foundmember",foundmember)
          return foundmember;
      
      }catch(error){
          console.log("error",error)
          throw new Error(err)
     }
    },
},
Mutation:{
    createchat:combineResolvers(isAuthenticated,async(parent,{data},{models,me},info) => {
        console.log("data",{data})

        try{
         return new models.chat({ ...data,userid:me._id}).save();
    
    } catch (error) {
        console.log('error', error);
        return error;
    }
}),
updatechat:combineResolvers(isAuthorizedchat, async (parent, { _id, data }, { models}) => {
    try {
        const fields = ['message'];
        const updatedchatDoc = await models.chat.findById(_id);
        for (let i = 0; i < fields.length; i += 1) {
            if (data[fields[i]]) updatedchatDoc[fields[i]] = data[fields[i]];
        }
        updatedchatDoc.updatedDate = new Date();
        console.log("updatedchatDoc",updatedchatDoc)
        return updatedchatDoc.save();
    } catch (error) {
        console.log('error', error);
        return error;
    }
}),
Deletechat:combineResolvers(isAuthorizedchat,async(parent,{_id},{models})=>{
    
    try{
        let deletedchat=await models.chat.deleteOne({_id})
        console.log("deletedchat",deletedchat)
        return deletedchat;
    
    }catch(error){
        console.log("error",error)
        return error;
   }
  }),
},
};

export default chatResolvers;