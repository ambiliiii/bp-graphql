import { gql } from 'apollo-server-express';

export default gql`
type Chat{
    _id:ID!
    userid:ID
    groupid:ID
    message:String
    created_date:String!
    Member:[Member]
}

extend type Query{
    listchat:[Chat]!
    getchat(_id:ID!):Chat!
}
input chatInput{
    groupid:ID
    message:String
    
}
input updatechatInput{
    message:String
}


extend type Mutation {
    createchat(data:chatInput):Chat!
    updatechat(_id:ID!,data:updatechatInput):Chat!
    Deletechat(_id:ID):Chat!

}



`;
