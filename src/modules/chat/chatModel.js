import mongoose from 'mongoose';

const { Schema } = mongoose;
const chatSchema = new Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
    },

    userid:{
        type:mongoose.Schema.Types.ObjectId,required:true
    },
    groupid:{
        type:mongoose.Schema.Types.ObjectId,required:true
    },
    message:{
        type: String,
        required: true,
    },
    created_date:{
        type: Date,
        default: Date.now,
        index: true,
    },
    

})

export default mongoose.model('chat', chatSchema, 'chat');
