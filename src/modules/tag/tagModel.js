import mongoose from 'mongoose';

const { Schema } = mongoose;
const tagSchema = new Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
    },
    tag:{
        type:String,required:true
    }
})

export default mongoose.model('tag', tagSchema, 'tag');
