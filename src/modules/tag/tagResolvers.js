import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { combineResolvers } from 'graphql-resolvers';
import { isAuthenticated, isAdmin } from '../auth';
import emailTemplate from '../email/email-service';
import { models } from 'mongoose';
import post from '.';
import user from '../user';
import comment from '.';
import tag from '.';
const tagResolvers = {
    Query: {
        listtag:async(parent,args,{models})=>{
            try{
            const listedtag = await models.tag.find({})
            console.log("listedtag",listedtag)
            return listedtag;
            }
         catch (error) {
            console.log('error', error);
            return error;
        }
 },
},

Mutation:{
    createtag:(parent,{data},{models},info) => {
        console.log("data",{data})

        try{
         return new models.tag({ ...data}).save();
    
    } catch (error) {
        console.log('error', error);
        return error;
    }
},
},
 };
export default tagResolvers;
