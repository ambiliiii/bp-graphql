import tagModel from './tagModel';
import tagSchema from './tagSchema';
import tagResolvers from './tagResolvers';

export default {
    tagModel,
    tagSchema,
    tagResolvers
};
