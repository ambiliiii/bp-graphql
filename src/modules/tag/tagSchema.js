import { gql } from 'apollo-server-express';

export default gql`

type Tag{
    _id:ID!
    tag:String!
}
extend type Query {
    listtag:[Tag]!

}

input tagInput{
    tag:String!

}


extend type Mutation {
    createtag(data:tagInput):Tag!
}


`;