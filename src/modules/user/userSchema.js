import { gql } from 'apollo-server-express';

export default gql`
    enum Role {
        user
        admin
    }

    type User {
        _id: ID
        firstName: String
        lastName: String
        email: String
        phoneNumber: String
        status: String
        role: String
        designationid:ID
        point:Int
        Post:[Post]
        getDesignation:[Designation]
        getRank:Point

    }

    extend type Query {
        listUser(skip: Int, limit: Int): [User]!
        me:User!
       
    }

    type UpdateStatus {
        status: Boolean!
        message: String!
    }

    input createUserInput {
        firstName: String!
        lastName: String!
        email: String!
        phoneNumber: String!
        password: String!
        designationid:ID!

    }

    input updateUserInput {
        firstName: String!
        lastName: String!
        email: String!
        phoneNumber: String!
    }

    type login {
        token: String!
    }
    extend type Mutation {
        login(email: String!, password: String!): login!
        forgotPassword(email: String!): UpdateStatus!
        changePassword(currentPassword: String!, newPassword: String!): UpdateStatus!
        createUser(data: createUserInput!): User!
        updateUser(_id: ID!, data: updateUserInput!): User!
    }
`;
