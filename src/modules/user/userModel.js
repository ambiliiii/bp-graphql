import mongoose from 'mongoose';
//import uniqueValidator from 'mongoose-unique-validator';

const { Schema } = mongoose;

const UserSchema = new Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
    },
    designationid:{
        type: mongoose.Schema.Types.ObjectId,required:true
    },
    firstName: {
        type: String,
        required: true,
        trim: true,
    },
    lastName: {
        type: String,
        required: true,
        trim: true,
    },
    email: {
        type: String,
        required: true,
        lowercase: true,
        trim: true,
        index: true,
        //unique: true,
    },
    phoneNumber: {
        type: String,
        required: false,
    },
    password: {
        type: String,
        required: true,
        trim: true,
    },
    status: {
        type: Boolean,
        default: true,
        index: true,
    },
    resetPasswordToken: {
        type: String,
        required: false,
        default: null,
    },
    role: {
        type: String,
        enum: ['admin', 'user'],
        default: 'user',
    },
    createdDate: {
        type: Date,
        default: Date.now,
        index: true,
    },
    updatedDate: {
        type: Date,
        default: Date.now,
        index: true,
    },
    point: {
        type:Number
    }
});

//UserSchema.plugin(uniqueValidator);

export default mongoose.model('User', UserSchema, 'User');
