/* eslint-disable no-console */
/* eslint-disable security/detect-object-injection */

import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { combineResolvers } from 'graphql-resolvers';
import { isAuthenticated, isAdmin } from '../auth';
import emailTemplate from '../email/email-service';
import { models } from 'mongoose';
import user from '.';
import post from '.';
var ObjectId = require('mongodb').ObjectID;

const UserResolvers = {
    Query: {
        listUser: combineResolvers(isAdmin, async (parent, args, { models }) => {
            const data = await models.user.find(
                { status: true },
                { _id: 1, firstName: 1, lastName: 1, email: 1, phoneNumber: 1, gender: 1, status: 1 ,designationid:1}
            );
            return data;
        }),
       
        me:combineResolvers(isAuthenticated,async (parent,args,context)=>{
            console.log("context.me",context.me)


            return context.me;
        })
    },
    User:{
        Post:async(parent,args,{models})=>{
          console.log("parent",parent)
          try{
              let foundpost=await models.post.find({userid:parent._id})
              console.log("foundpost",foundpost)
              return foundpost;
          
          }catch(error){
              console.log("error",error)
              throw new Error(err)
         }
        }
         },
    Mutation: {
        login: async (parent, { email, password }, { models }) => {
            const findUser = await models.user.findOne({ email }, { _id: 1, password: 1 });
            if (!findUser) return new Error('User does not exist');

            const isEqual = await bcrypt.compare(password, findUser.password);
            if (!isEqual) return new Error('Email/Password does not match');

            const token = jwt.sign({ userId: findUser._id }, process.env.JWT_SECRET, {
                expiresIn: process.env.JWT_EXPIRY,
            });
            return { token };
        },
        createUser: async (parent, { data }, { models }) => {
            try {
                const hashedPassword = await bcrypt.hash(
                    data.password,
                    Number(process.env.BCRYPT_PASSWORD_HASH_ROUNDS)
                );
                return new models.user({ ...data, password: hashedPassword }).save();
            } catch (error) {
                console.log('error', error);
                return error;
            }
        },
        updateUser: combineResolvers(isAuthenticated, async (parent, { _id, data }, { models }) => {
            try {
                const fields = ['firstName', 'lastName', 'email', 'phoneNumber'];
                const registrationDoc = await models.user.findById(_id);
                for (let i = 0; i < fields.length; i += 1) {
                    if (data[fields[i]]) registrationDoc[fields[i]] = data[fields[i]];
                }
                registrationDoc.updatedDate = new Date();
                return registrationDoc.save();
            } catch (error) {
                console.log('error', error);
                return error;
            }
        }),
        forgotPassword: async (parent, { email }, { models }) => {
            try {
                const foundUser = await models.user.findOne({ email, status: true }, { firstName: 1, lastName: 1 });
                if (!foundUser) return new Error('User does not exist');

                const resetPasswordToken = jwt.sign({ userId: foundUser._id }, process.env.JWT_SECRET, {
                    expiresIn: process.env.JWT_FORGET_PASSWORD_EXPIRY,
                });
                foundUser.resetPasswordToken = resetPasswordToken;
                await foundUser.save();
                const name = foundUser.firstName;
                await emailTemplate(
                    'forgotPassword',
                    { name, url: `${process.env.CLIENT_WEB_APP_URL}/#/resetPasswordToken` },
                    email
                );
                return { status: true, message: `Password reset email has been sent to ${email}` };
            } catch (error) {
                console.log('error', error);
                return error;
            }
        },
        changePassword: combineResolvers(
            isAuthenticated,
            async (parent, { currentPassword, newPassword }, { models, me: { _id } }) => {
                try {
                    const findUser = await models.user.findById(_id, { password: 1 });
                    if (!findUser) return new Error('User does not exist');

                    const isEqual = await bcrypt.compare(currentPassword.trim(), findUser.password);
                    if (!isEqual) return new Error('Password is incorrect');

                    const hashedPassword = await bcrypt.hash(
                        newPassword.trim(),
                        Number(process.env.BCRYPT_PASSWORD_HASH_ROUNDS)
                    );
                    findUser.password = hashedPassword;
                    findUser.resetPasswordToken = null;
                    await findUser.save();

                    return { status: true, message: 'Password has been updated successfully' };
                } catch (error) {
                    console.log('error', error);
                    return error;
                }
            }
        ),
    },
    User:{
        getDesignation:async(parent,args,{models})=>{
          console.log("parent",parent)
          try{
              let founddesignation=await models.designation.find({_id:parent.designationid})
              console.log("founddesignation",founddesignation)
              return founddesignation;
          
          }catch(error){
              console.log("error",error)
              return error;
         }
        },
        getRank:async(parent,args,{models})=>{
            //console.log("parent",parent)
            try{
        let rank = await models.points.aggregate([
            {
               $group: {
                    _id:"$userid",
                    totalPoint: { "$sum": "$point" },
                
            }},
        
        { $sort: { totalPoint: -1 } },
       {
            $group: {
                _id: false,
                arr: {
                    $push: {
                        userid:"$_id",
                        totalPoint: '$totalPoint',
                    },
                   
                },
                
            },
        },
        {
            $unwind: {
                path: "$arr",
                includeArrayIndex: 'rank',
            },
        },
        { $match: { 'arr.userid': ObjectId(`${parent._id}`) } },
        { $set: { rank: { $add: ['$rank', 1] } } },
        { $project: { rank: 1, _id: 0} },
        
        ])
        
        console.log("rank",rank)
         return rank;
    }catch(error){
        console.log("error",error)
     return error;
   }
    }
    }
}

export default UserResolvers;
