import mongoose from 'mongoose';

const { Schema } = mongoose;
const notificationSchema = new Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
    },
    postid:{
        type:mongoose.Schema.Types.ObjectId,
    },
    receiverid:{
        type:mongoose.Schema.Types.ObjectId,
    },
    senderid:{
        type:mongoose.Schema.Types.ObjectId,
    },
    notification:{
        type:String
    },
    commentid:{
        type:mongoose.Schema.Types.ObjectId,
    },
    likeid:{
        type:mongoose.Schema.Types.ObjectId,
    },
    firstName:{
        type:String
    },
    lastName:{
        type:String
    },

})

export default mongoose.model('notification', notificationSchema, 'notification');
