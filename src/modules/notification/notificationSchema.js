import { gql } from 'apollo-server-express';

export default gql`

type Notification{
    _id:ID!
    receiverid:ID
    senderid:ID
    postid:ID
    commentid:ID
    likeid:ID
    notification:String
   
}

input noteInput{
    userid:ID
    postid:ID
    notification:String
}


extend type Mutation {
    createnotification(data:noteInput):Notification!
}

`;