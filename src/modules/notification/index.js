import notificationModel from './notificationModel';
import notificationSchema from './notificationSchema';
import notificationResolvers from './notificationResolvers';

export default {
    notificationModel,
    notificationSchema,
    notificationResolvers,

};