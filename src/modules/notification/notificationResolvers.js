import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { combineResolvers } from 'graphql-resolvers';
//import { isAuthenticated, isAdmin } from '../auth';
import emailTemplate from '../email/email-service';
import { models } from 'mongoose';
//import user from '.';
import post from '.';
import user from '../user';



const notificationResolvers = {
   
 Mutation:{
    createnotification: (parent,{data},context,info) => {
        try{
         return new models.notification({ ...data}).save();
    
    } catch (error) {
        console.log('error', error);
        return error;
    }
},
},


};       
export default notificationResolvers;