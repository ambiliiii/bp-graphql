import groupModel from './groupModel';
import groupSchema from './groupSchema';
import groupResolvers from './groupResolvers';

export default {
    groupModel,
    groupSchema,
    groupResolvers,

};