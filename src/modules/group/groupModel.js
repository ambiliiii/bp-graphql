import mongoose from 'mongoose';

const { Schema } = mongoose;
const groupSchema = new Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
    },
    memberid:{
        type: mongoose.Schema.Types.ObjectId,
    },

    groupName:{
        type: String,
        required: true,
    },
    created_date:{
        type: Date,
        default: Date.now,
        index: true,
    },
    

})

export default mongoose.model('group', groupSchema, 'group');
