import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { combineResolvers } from 'graphql-resolvers';
import { isAuthenticated, isAdmin} from '../auth';
import emailTemplate from '../email/email-service';
import { models } from 'mongoose';
//import post from '.';
import user from '../user';
import comment from '.';
import group from '.';


const groupResolvers = {
    Query: {
        listgroup:combineResolvers(isAuthenticated,async(parent,args,{models})=>{
            try{
            const listedgroup = await models.group.find({})
            console.log("listedgroup",listedgroup)
            return listedgroup;
            }
         catch (error) {
            console.log('error', error);
            return error;
        }
 }),
},
Group:{
    Member:async(parent,args,{models})=>{
      console.log("parent",parent)
      try{
          let foundmember=await models.member.find({groupid:parent._id})
          console.log("foundmember",foundmember)
          return foundmember;
      
      }catch(error){
          console.log("error",error)
          throw new Error(err)
     }
    },
    Chat:async(parent,args,{models})=>{
        console.log("parent",parent)
        try{
            let foundchats=await models.chat.find({groupid:parent._id})
            console.log("foundchats",foundchats)
            return foundchats;
        
        }catch(error){
            console.log("error",error)
            throw new Error(err)
       }
      },

     },
Mutation:{
    creategroup:combineResolvers(isAuthenticated,async(parent,{data},{models,me},info) => {
        console.log("data",{data})

        try{
         return new models.group({ ...data}).save();
    
    } catch (error) {
        console.log('error', error);
        return error;
    }
}),
updategroup:async (parent, { _id, data }, { models}) => {
    try {
        const fields = ['groupName'];
        const updatedgroupDoc = await models.group.findById(_id);
        for (let i = 0; i < fields.length; i += 1) {
            if (data[fields[i]]) updatedgroupDoc[fields[i]] = data[fields[i]];
        }
        updatedgroupDoc.updatedDate = new Date();
        console.log("updatedgroupDoc",updatedgroupDoc)
        return updatedgroupDoc.save();
    } catch (error) {
        console.log('error', error);
        return error;
    }
},
Deletegroup:async(parent,{_id},{models})=>{
    
    try{
        let deletedgroup=await models.group.deleteOne({_id})
        console.log("deletedgroup",deletedgroup)
        return deletedgroup;
    
    }catch(error){
        console.log("error",error)
        return error;
   }
  },
},
};

export default groupResolvers;