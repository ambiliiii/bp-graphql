import { gql } from 'apollo-server-express';

export default gql`
type Group{
    _id:ID
    groupName:String
    created_date:String
    Member:[Member]!
    Chat:[Chat]!
}

extend type Query{
    listgroup:[Group]!
}
input chatGroupInput{
    groupName:String!
    
}
input updategroupInput{
    groupName:String
}


extend type Mutation {
    creategroup(data:chatGroupInput):Group!
    updategroup(_id:ID!,data:updategroupInput):Group!
    Deletegroup(_id:ID):Group!

}



`;
