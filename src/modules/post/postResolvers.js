import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { combineResolvers } from 'graphql-resolvers';
import { isAuthenticated, isAdmin,isAuthorizedpostowner} from '../auth';
import emailTemplate from '../email/email-service';
import { models } from 'mongoose';
//import user from '.';
import post from '.';
import user from '../user';
import { text } from 'express';



const postResolvers = {
    Query: {
        listpost:combineResolvers(isAuthenticated,async(parent,args,{models})=>{
            try{
            const listedpost = await models.post.find({})
            console.log("listedpost",listedpost)
            return listedpost;
            }
         catch (error) {
            console.log('error', error);
            return error;
        }
 }),
 getpost:combineResolvers(isAuthenticated,async(parent,args,context,info)=>{
    try{
            const gotpost = await models.post.findById({_id:args._id});
              return gotpost;
    } catch (error) {
        console.log('error', error);
        return error; 
    }
 }),
 listpostcount:combineResolvers(isAuthenticated,async(parent,args,{models})=>{
    try{
        const listedpostcount= await models.post.find({}).count();
        console.log("listedpostcount",listedpostcount)
          return listedpostcount;
} catch (error) {
    console.log('error', error);
    return error; 
}
 }),
    },
Post:{
    User:async(parent,args,{models})=>{
      //console.log("parent",parent)
      try{
          let foundUser=await models.user.find({_id:parent.userid})
          console.log("foundUser",foundUser)
          return foundUser;
      
      }catch(error){
          console.log("error",error)
          throw new Error(err)
     }
    },

     },
     
   Mutation:{
        createpost:combineResolvers(isAuthenticated,async (parent,{data},{models,me},info) => {
            try{
                //console.log("data",data)
                //console.log("context.me",me)
             let postdata=await models.post({ ...data,userid:me._id}).save();
            console.log("postowner",postdata)  

            if (data.postType==="text") {
                let pointofotext=await models.points({
                    point:1,
                    postid:postdata._id,
                    userid:postdata.userid,
                
              }).save();

                 } else if(data.postType=="photo") {
                let pointofphoto= await models.points({
                    point:2,
                    postid:postdata._id,
                    userid:postdata.userid,
                
              }).save();
            }
            else{
                let pointofvideo= await models.points({
                    point:3,
                    postid:postdata._id,
                    userid:postdata.userid,
                
              }).save();
            }
            let createnotification=await models.notification({
                senderid:me._id,
                postid:postdata._id,
                notification:`${me.firstName} ${me.lastName} added new post`
            }).save();
            console.log("createnotification",createnotification)
               return postdata
        
        } catch (error) {
            console.log('error', error);
            return error;
        }
    }),
    
    updatepost:combineResolvers(isAuthorizedpostowner, async (parent, { _id, data }, { models }) => {
        try {
            const fields = ['title', 'content'];
            const updatedpostDoc = await models.post.findById(_id);
            for (let i = 0; i < fields.length; i += 1) {
                if (data[fields[i]]) updatedpostDoc[fields[i]] = data[fields[i]];
            }
            updatedpostDoc.updatedDate = new Date();
            return updatedpostDoc.save();
        } catch (error) {
            console.log('error', error);
            return error;
        }
    }),
    Deletepost:combineResolvers(isAuthorizedpostowner,async(parent,{_id},{models})=>{
    
        try{
            let deletedpost=await models.post.deleteOne({_id})
            console.log("deletedpost",deletedpost)
            return deletedpost;
        
        }catch(error){
            console.log("error",error)
            return error;
       }
      }),
},

Post:{
    getTag:async(parent,args,{models})=>{
      console.log("parent",parent)
      try{
          let foundTag=await models.tag.find({_id:{$in:parent.tagid}})
          console.log("foundTag",foundTag)
          return foundTag;
      
      }catch(error){
          console.log("error",error)
          throw new Error(err)
     }
    },
    
}
};
        
    export default postResolvers;