import { gql } from 'apollo-server-express';

export default gql`
enum PostType {
    photo
    video
    text
}
type Post{
    _id:ID
    userid:ID
    postType:String
    created_date:String
    title:String
    content:String
    tagid:ID
    User:[User]!
    getTag:[Tag]
    point:Int
    
}

extend type Query {
    getpost(_id:ID!):Post!
    listpost:[Post]!
    listpostcount:Post
    
}
input postInput{
    postType:String
    title:String!
    content:String!
    tagid:ID
}
input updatepostInput{
    title:String!
    content:String!
}


extend type Mutation {
    createpost(data: postInput!): Post!
    updatepost(_id:ID!,data:updatepostInput):Post!
    Deletepost(_id:ID):Post!
   

}
`;



