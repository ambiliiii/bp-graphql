import postModel from './postModel';
import postSchema from './postSchema';
import postResolvers from './postResolvers';

export default {
    postModel,
    postSchema,
    postResolvers,

};