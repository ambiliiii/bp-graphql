import mongoose from 'mongoose';

const { Schema } = mongoose;
const postSchema = new Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
    },
     userid:{
         type:mongoose.Schema.Types.ObjectId,required:true
     },
     tagid:{
         type:mongoose.Schema.Types.ObjectId
     },
    postType: {
        type: String,
        enum: ['photo', 'video','text'],
        default: 'text',
    },
    title:{
        type: String,
        required: true,
        trim: true,
    },
    content:{
        type: String,
        required: true,  
    },
    created_date:{
        type: Date,
        default: Date.now,
        index: true,
    },
    point: {
        type:Number
    }
})

export default mongoose.model('post', postSchema, 'post');
