import { gql } from 'apollo-server-express';
import user from './user';
import post from'./post';
import comment from './comment';
 import points from'./points';
import designation from'./designation';
 import tag from'./tag';
 import like from'./like';
 import group from'./group';
import chat from './chat';
import member from './member';
import media from './media';
import notification from './notification';

const linkSchema = gql`
    type Query {
        _: Boolean
    }

    type Mutation {
        _: Boolean
    }

    type Subscription {
        _: Boolean
    }
`;

const models = {
    user: user.UserModel,
    post:post.postModel,
    group:group.groupModel,
     comment:comment.commentModel,
     designation:designation.designationModel,
     like:like.likeModel,
     points:points.pointModel, 
     tag:tag.tagModel,
     chat:chat.chatModel,
     member:member.memberModel,
     media:media.mediaModel,
     notification:notification.notificationModel
};

export default {
    models,
    schema: [linkSchema, user.UserSchema,
        group.groupSchema,
        post.postSchema,
         comment.commentSchema,
         designation.designationSchema,
        like.likeSchema,
        tag.tagSchema,
        chat.chatSchema,
        member.memberSchema,
     points.pointSchema,
     media.mediaSchema,
     notification.notificationSchema
],
    resolvers: [user.UserResolver,
        post.postResolvers,
        comment.commentResolvers,
        like.likeResolvers,tag.tagResolvers,
        designation.designationResolvers,
        group.groupResolvers,
    chat.chatResolvers,
member.memberResolvers,
points.pointResolvers,
media.mediaResolvers,
notification.notificationResolvers],
    loaders: {
        user: (keys) => user.userLoader.batchUsers(keys, models),
    },
};
